# This Modul returns the count of all running processes

import psutil

def processesCountTest():

    processes = []

    # Save pid for every process in list to count them afterwards
    for process in psutil.process_iter(['pid']):
        processes += process.info

    countProcesses = len(processes)
        
    return countProcesses
