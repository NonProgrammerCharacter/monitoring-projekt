#!/usr/bin/env python

# This script can monitor cpu load, ram usage and process count.
# To use this script you have to install the "psutil" module and place the modules "processes.py, cpu.py and ram.py" next to the script
# To change the limits for the different tests use the "monitoring_limits" file next to it
# Usage: python3 alarm.py (-p -c -m -l)
#   -p [Switch]: Activates the process count test
#   -c [Switch]: Activates the cpu load test
#   -m [Switch]: Activates the ram usage test
#   -l [Switch]: Switches the output from console to logfile, the logfile will be placed next to the script with the name "monitoring.log"

import psutil, argparse, datetime

# Self made modules
import processes, cpu, limits

#  Commandline argument definition
parser = argparse.ArgumentParser()
parser.add_argument("-c", "--cpu", action="store_true", default="False", help="Gibt CPU Auslastung aus.")
parser.add_argument("-m", "--ram", action="store_true", default="False", help="Gibt RAM Belegung aus.")
parser.add_argument("-p", "--processes", action="store_true", default="False", help="Gibt Anzahl der derzeit laufenden Prozesse aus.")
parser.add_argument("-log", "--log", action="store_true", default="False", help="Ausgabe wird in Logdatei geschrieben anstatt in der Konsole ausgegeben.")
args = parser.parse_args()

# Output function used by tests for writing to console or logfile
def ausgabe(warning, mail):
    if args.log == True:
        
        # Authentication wont work with my mail so to not get errors all the time but still have the email function I set this variable
        email = False

        # Get date and time
        dateAndTime = datetime.datetime.now()

        # Format date and time to european standard format
        dateAndTime = dateAndTime.strftime("%d-%m-%Y %H:%M:%S")
        formatedOutput = f"{dateAndTime:22}{warning}\n"
       
        if email == True:
            mailAusgabe(formatedOutput)

        # Better control over opening and closing of a file even with errors
        with open("monitoring.log", "a") as logFile:

            # Write to logfile
            logFile.write(formatedOutput)
    else:
        
        # Write to console
        print(warning)

# Function for mail sending
def mailAusgabe(warning):
    # Read mail login, password and receiver from file
    with open('/root/.mailinfo') as mailInfo:
        senderAddress = mailInfo.readline()
        receiverAddress = mailInfo.readline()
        password = mailInfo.readline()
    
    # Remove newline in variable
    senderAddress = senderAddress.rstrip("\n")
    receiverAddress = receiverAddress.rstrip("\n")
    password = password.rstrip("\n")
    
    port = 587        
    smtpServer = "smtp.web.de"
    message =   f"""\From: {senderAddress}
                    rcpt To: {receiverAddress}
                    DATA
                    To: {receiverAddress}
                    From: {senderAddress}
                    Subject: Monitoring Problem

                    {warning}

                    .
                    """

    context = ssl.create_default_context()

    # Connect to mailserver and send mail
    with smtplib.SMTP(smtpServer, port) as server:

        server.ehlo()    
        server.starttls(context=context)
        server.ehlo()
        server.login(senderAddress, password)
        server.sendmail(senderAddress, receiverAddress, message)

# CPU load test
if args.cpu == True:
    
    # Use "cpuLoadTest" from cpu modulee with custom limits
    cpuTestResult = cpu.cpuLoadTest()
    
    # Compare cpu load with limit valuees and define warning message if cpu load is to high
    if cpuTestResult > limits.cpuHighLimit:
        cpuWarning = f"Fehler: hohe CPU Auslastung, Auslastung beträgt: {cpuTestResult:.2f}%"
        ausgabe(cpuWarning, True)
    elif cpuTestResult > limits.cpuLowLimit:
        cpuWarning = f"Warnung: CPU Auslastung beträgt: {cpuTestResult:.2f}%"
        ausgabe(cpuWarning, False)

# RAM usage test
if args.ram == True:

    # Get ram rusage
    ramUsage = psutil.virtual_memory().percent

    # Compare ram usage with limit values and define warning message if ram usage to high
    if ramUsage > limits.ramHighLimit:
        ramWarning = f"Fehler: hohe RAM-Belegung, RAM-Belegung beträgt: {ramUsage}%"
        ausgabe(ramWarning, True)
    elif ramUsage > limits.ramLowLimit:
        ramWarning = f"Warnung: RAM-Belegung beträgt: {ramUsage}%"
        ausgabe(ramWarning, False)

# Process count test
if args.processes == True:
    # Use "processCountTest" from processes module with custom limits
    procTestResult = processes.processesCountTest()

    # Compare process count with limit values and define warning message if process count is to high
    if procTestResult > limits.processesHighLimit:
        processWarning = f"Fehler: sehr viele Prozesse laufen: {procTestResult} Prozesse"
        ausgabe(processWarning, True)
    elif procTestResult > limits.processesLowLimit:
        processWarning = f"Warnung: viele Prozesse laufen: {procTestResult} Prozesse"
        ausgabe(processWarning, False)
