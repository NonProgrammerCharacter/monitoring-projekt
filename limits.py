# Config module for alarm.py

cpuLowLimit = 60
cpuHighLimit = 80

ramLowLimit = 60
ramHighLimit = 80

processesLowLimit = 60
processesHighLimit = 100
