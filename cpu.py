# This module checks the cpu load with custom limits and returns it

import psutil

# Declare list-variable for later use in hope of fixxing the values not getting saved
averageCpuLoad = []

# CPU load test
def cpuLoadTest():


    # Declare list-variable for later use in hope of fixxing the values not getting saved
    averageCpuLoad = []

    cpuLoad = (psutil.cpu_percent(interval=0.1))

    # Insert one value to fix an error which happens if cpu_percent only gets 0.0 values which will be filtered out
    averageCpuLoad.append(cpuLoad)

    # Needed because the "cpu_percent" command compares the values to get the cpu load and does not read it from the system directly
    for i in range(100):

        cpuLoad = (psutil.cpu_percent(interval=0.1))

        # cpu_percent gives sometimes bad values (0.0) so I have to filter them out
        if cpuLoad > 0.1:
            averageCpuLoad.append(cpuLoad)

    # Calculate average cpu load because the list has multiple values atm.
    averageCpuLoad = float(sum(averageCpuLoad))/len(averageCpuLoad)
    return averageCpuLoad
